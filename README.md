extdirect.django
====

extdirect.django allows you to generate ExtJS components from your django models using ExtDirect api

**Additions**:

 * Generate editable and tunables datagrids from your django models
 * Expose your models with CRUD
 * Form and ModelForm generation

**Requirements**:

 * [ExtJs][2] >= 1.3
 * [Django][3] >= 1.2
 * [Ext.ux.AwesomeCombo][4]

**Doc :**

  [1]: https://github.com/gsancho/extdirect.django
  [2]: http://sencha.com/products/extjs
  [3]: http://www.djangoproject.com
