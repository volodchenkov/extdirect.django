from datetime import datetime

from django.conf.urls import patterns
from django.conf.urls import url
from django.utils import timezone

from extdirect.django.extfields import DateTimeField


def extdirect_patterns(remote_provider, namespace):
    """
    Helper function to return a URL pattern for serving extdirect js files.

    urlpatterns = patterns('',
        # ... the rest of your URLconf goes here ...
    ) + extdirect_patterns(direct.remote_provider, 'namespace_name')

    """
    return patterns('',
        url(r'^router$', remote_provider.router,
            name='{0}-direct-router'.format(namespace)),

        url(r'^provider.js$', remote_provider.script,
            name='{0}-direct-provider'.format(namespace)),

        url(r'^api$', remote_provider.api,
            name='{0}-direct-api'.format(namespace)),
    )


def parseDateTime(value):
    """
    Parses string to datetime object. Throws TypeError and ValueError.
    """
    if value:
        value = datetime.strptime(value, DateTimeField.FORMAT_PARSE)
        value = timezone.make_aware(value, timezone.get_current_timezone())
    return value
