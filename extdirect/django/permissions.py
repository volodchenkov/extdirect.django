from importlib import import_module
import inspect

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ImproperlyConfigured
from django.db import models
from django.db.models import signals


def create_read_permissions(app, verbosity):
    """
    Adds `read` permissions to all installed apps on syncdb.
    """
    perms = [] # list of read permissions, that should exist
    content_types = set()

    for klass in models.get_models(app):
        ct = ContentType.objects.get_for_model(klass)
        name = u'Can read {}'.format(klass._meta.verbose_name_raw)
        key = u'read_{}'.format(klass._meta.object_name.lower())
        perms.append(Permission(name=name, codename=key, content_type=ct))
        content_types.add(ct)

    qs = Permission.objects.filter(content_type__in=content_types)
    existing_perms = set(qs.values_list("content_type", "codename"))

    missing_perms = [p for p in perms
                     if (p.content_type.pk, p.codename) not in existing_perms]
    Permission.objects.bulk_create(missing_perms)
    if verbosity >= 1:
        for obj in missing_perms:
            print "Adding permission '%s'" % obj


def create_permissions(silent=False):
    """
    For all apps tries to import `remote_provider` from direct.py and
    saves to DB all provider's permissions. Also adds `read`
    permssions.
    """
    for app in models.get_apps():
        create_read_permissions(app, verbosity=2)

        app_models = models.get_models(app)
        app_name = app_models[0]._meta.app_config.name if app_models else None
        try:
            module = import_module('{}.direct'.format(app_name))
            providers = [obj for name, obj in inspect.getmembers(module)
                         if 'remote_provider' in name]
            actions = []
            for provider in providers:
                actions.extend(getattr(provider, 'actions', {}).values())
        except ImportError:
            continue

        perms = set()
        for action in (a for action in actions for a in action.values()):
            permission = action.get('permission')
            if permission:
                perms.add(permission)

        for perm in perms:
            app_name, codename = perm.split('.')
            try:
                app = models.get_app(app_name)
            except ImproperlyConfigured:
                pass
            content_types = [ContentType.objects.get_for_model(m)
                             for m in models.get_models(app)]

            qs = Permission.objects.filter(content_type__in=content_types)
            try:
                obj, created = qs.get_or_create(codename=codename, defaults={
                        'name': "Can {}".format(codename.replace("_", " ")),
                        'content_type': content_types[0]
                })
            except Permission.MultipleObjectsReturned:
                created = False

            if created and not silent:
                print "Added permission '{}'".format(obj.name)


def post_migrate_handler(app_config, verbosity, **kwargs):
    create_read_permissions(app_config.models_module, verbosity)

signals.post_migrate.connect(post_migrate_handler)
