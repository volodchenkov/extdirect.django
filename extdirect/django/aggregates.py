from datetime import timedelta

from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext as _
from extdirect.django import utils


def process_by_date(request, queryset, record_builder, default_interval=None):
    """
    Helper view, that aggregates queryset objects by date with
    specified `interval` in seconds. `record_builder` must be a
    callable that returns dict corresponding to record in extjs
    recordset. It will be called for each interval:

        record_builder(queryset, begin, end)

    `interval` and two datetime parameters `from` and `to` should be
    passed from extjs. Handles filtering ignoring sotring and pagination.
    """
    ext_params = request.extdirect_post_data[0]

    interval = ext_params.get('interval') or default_interval
    try:
        interval = timedelta(seconds=interval)
    except TypeError:
        return dict(success=False, message=_('Incorrect interval.'))

    # ignoring extjs sorting and pagination params
    filter_params = dict((k, v) for k, v in ext_params.iteritems()
                         if k not in ('from', 'to', 'interval', 'start',
                                      'limit', 'page', 'sort', 'dir'))
    queryset = queryset.filter(**filter_params)

    try:
        date_from = utils.parseDateTime(ext_params.get('from'))
        date_to = utils.parseDateTime(ext_params.get('to'))
    except ValueError, TypeError:
        return dict(success=False, message=_('Missing or incorrect '
                                             '"from" and "to" parameters.'))

    result = []
    begin = date_from
    end = min(date_from + interval, date_to)
    while begin < date_to:
        rec = record_builder(queryset, begin, end)
        if rec:
            result.append(rec)
        begin += interval
        end = min(end + interval, date_to)

    return result
